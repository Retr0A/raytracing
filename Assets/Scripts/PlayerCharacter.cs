using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]

public class PlayerCharacter : MonoBehaviour
{
    public Vector3 moveDirection = Vector3.zero;

    [Space]
    public float walkingSpeed = 3.0f;
    public float runningSpeed = 5.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public Camera playerCamera;
    public float lookSpeed = 2.0f;
    public float lookXLimit = 90.0f;

    [HideInInspector]
    public CharacterController characterController;
    float rotationX = 0;

    [HideInInspector]
    public bool canMove = true;
    public bool canInput = true;

    void Start()
    {
        characterController = GetComponent<CharacterController>();

        Cursor.lockState = CursorLockMode.Locked;

        float locWalkingSpeed = walkingSpeed;
        float locRunningSpeed = runningSpeed;
    }

    void Update()
    {
        if (canInput)
        {
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            Vector3 right = transform.TransformDirection(Vector3.right);

            bool isRunning = Input.GetKey(KeyCode.LeftShift);
            float curSpeedX = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Vertical") : 0;
            float curSpeedY = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Horizontal") : 0;
            float movementDirectionY = moveDirection.y;
            moveDirection = (forward * curSpeedX) + (right * curSpeedY);

            if (false)
            {
                if (Input.GetMouseButton(0))
                {
                    //equippedTool.gameObject.GetComponent<Animator>().SetBool("ShouldShoot", true);

                    //StartCoroutine(EnableMuzzleFlash());

                    RaycastHit hit;
                    if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, Mathf.Infinity))
                    {
                        /*if (!hitSoundEffect.isPlaying)
                        {
                            hitSoundEffect.Play();
                        }/*

                        if (true)//hit.transform.gameObject.GetComponent<DestructiableObject>())
                        {
                            /*hit.transform.gameObject.GetComponent<DestructiableObject>().DestroyObject(selectedBuildItem.shouldScaleDestroyedObject,
                                selectedBuildItem.destroyedObjectScaleOffset,
                                selectedBuildItem.destructedRotationOffset,
                                selectedBuildItem.name);*/
                    }

                    if (hit.transform.gameObject.tag == "BrokenPart")
                    {
                        //equippedTool.OnShoot(hit);

                        /*if (hit.transform.gameObject.GetComponent<BrokenPart>().shouldPlayHitEffect)
                        {
                            Instantiate(hit.transform.gameObject.GetComponent<BrokenPart>().hitParticle, hit.point, Quaternion.identity);
                        }*/
                    }
                }
            }
            else
            {
                //equippedTool.gameObject.GetComponent<Animator>().SetBool("ShouldShoot", false);
            }

            if (Input.GetMouseButtonDown(0))
            {
            }

            if (Input.GetMouseButtonUp(0) && false)
            {
                //false = false;
            }

            if (Input.GetKeyDown(KeyCode.C) || Input.GetKeyDown(KeyCode.LeftControl))
            {
                characterController.height = 0.5f;
            }

            if (Input.GetKeyUp(KeyCode.C) || Input.GetKeyUp(KeyCode.LeftControl))
            {
                characterController.height = 1.5f;
            }

            if (Input.GetButton("Jump") && canMove && characterController.isGrounded)
            {
                moveDirection.y = jumpSpeed;
            }
            else
            {
                moveDirection.y = movementDirectionY;
            }

            if (!characterController.isGrounded)
            {
                moveDirection.y -= gravity * Time.deltaTime;
            }


            characterController.Move(moveDirection * Time.deltaTime);

            if (characterController.velocity != new Vector3(0, 0, 0) || (Input.GetAxis("Mouse Y") != 0 || Input.GetAxis("Mouse X") != 0))
            {
                playerCamera.GetComponent<RayTracingManager>().numRenderedFrames = 0;
            }

            if (canMove)
            {
                rotationX += -Input.GetAxis("Mouse Y") * lookSpeed;
                rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
                playerCamera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
                transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * lookSpeed, 0);
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (true)//HJMapReader.Instance != null)
                {
                    if (true)
                    {
                        //GameController.Instance.ToggleBuildMenu();
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                //GameController.Instance.TogglePauseMenu();
            }

            if (Input.GetMouseButton(2))
            {
#if UNITY_EDITOR && false
				RaycastHit hit;
				if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, Mathf.Infinity))
				{
					DestructiableObject destructiableObject;
					if (hit.transform.gameObject.TryGetComponent<DestructiableObject>(out destructiableObject))
					{
						{
						if (destructiableObject.canBreak)
							if (!destructiableObject.isDestroyed)
							{
								destructiableObject.DestroyMesh();
							}
						}

                        Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward, Color.red, 10);
					}
				}
#endif
            }
        }
    }
}
